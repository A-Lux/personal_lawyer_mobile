import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personallawyer/finishedDocumentsPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'lang.dart';

class FinishedDocumentsScreen extends StatefulWidget {

  var title;

  FinishedDocumentsScreen({Key key, @required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FinishedDocumentsScreenState();
  }
}

class FinishedDocumentsScreenState extends State<FinishedDocumentsScreen> {

  var data;
  var url;
  var documents;
  List<Widget>myList = new List();
  ListView list;

  var isLoading = true;
  ScrollController _scrollController;
  var postsCount = 0;
  var contentIsDone = false;
  var dataD;

  var toggle = 0;
  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");
  }

  void getDocuments() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _api = prefs.getString("apikey");
    _lang = prefs.getString("language");

    url = "https://advokat.vipsite.kz/api/$_api/document/finished_documents/$postsCount/$_lang/";
    final response = await http.get(url);
    print(response.statusCode);

    documents = jsonDecode(response.body);
    dataD = jsonDecode(response.body);

    print(documents);

    setState(() {
      documents;
      dataD;
      isLoading = false;
    });

    if(response.statusCode == 200) {
      toggle++;
      for (int i = 0; i < documents.length; i++) {
        myList.add(
            new Column(
              children: <Widget>[
                ListTile(
                  title: Container(
                    padding: EdgeInsets.only(
                        top: 5, bottom: 5 ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          documents[i]["header_app_doc"], style: TextStyle(
                            fontWeight: FontWeight.bold ), ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 5 ),
                          child: Text(trans('order_date', _lang) + documents[i]["created"],
                            style: TextStyle(
                                color: Colors.grey, fontSize: 14 ), ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 5 ),
                          child: Row(
                            children: <Widget>[
                              Text(
                                trans('status', _lang), style: TextStyle(
                                  color: Colors.grey, fontSize: 14 ), ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: 5 ),
                                child: Text(
                                  documents[i]["status"], style: TextStyle(
                                    color: getColorFromHex(
                                        documents[i]["color"] ),
                                    fontSize: 14 ), ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  trailing: Container(
                    height: MediaQuery
                        .of(
                        context )
                        .size
                        .height / 2,
                    child: Icon(
                        Icons.arrow_forward_ios ),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                FinishedDocumentsPageScreen(
                                    id: documents[i]["id"],
                                    title: documents[i]["header_app_doc"] ) ) );
                  },
                ),

                Divider(height: 2.0, ),
              ],
            )

        );
      }
    } else {
      _scrollController..removeListener(_scrollListener);
      setState(() {
        contentIsDone = true;
      });

    }

  }

  @override
  void initState() {
    getLang();
    _scrollController = new ScrollController()..addListener(_scrollListener);
    getDocuments();
    super.initState();
  }


  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return Color(int.parse(hexColor, radix: 16));
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(widget.title, style: TextStyle(color: Colors.white)),
        ),

        body:  isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : SingleChildScrollView(
            controller: _scrollController,
            child: Container (
              padding: EdgeInsets.only(left: 1.0, right: 10.0, top: 15),
              child: Column(
                children: <Widget>[
                  Column(
                    children: myList,
                  ),

                 contentIsDone != 0 ?
                   toggle == 0 ?
                      Container(
                        child: Center(
                          child: Text(trans('documents_info', _lang)) ,
                        ),
                      )
                   :
                      Container()
                 :
                 Container(
                    margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    alignment: Alignment.bottomCenter,
                    child: CircularProgressIndicator(),
                 ),

                ],
              )
            )
        )
    );
  }

  void _scrollListener() {

    if(_scrollController.position.extentAfter < 100 && contentIsDone == false){
      print(_scrollController.position.extentAfter);

      setState(() {
        isLoading = false;
        getDocuments();
        postsCount += 20;
        // await
      });


    }
  }

}