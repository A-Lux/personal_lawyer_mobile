import 'package:flutter/material.dart';
import 'package:personallawyer/AskQuestion_step1.dart';
import 'package:personallawyer/orderDocument.dart';
import 'mainMenuBody.dart';
import 'navDrawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:share/share.dart';
import 'lang.dart';


class PagerScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PagerScreenState();
  }
}


class PagerScreenState extends State<PagerScreen> with SingleTickerProviderStateMixin{

  TabController tabC;
  var url;
  var data;
  var user;
  String _api;
  String _user_image = "";
  String _number;
  String _lang;

  void checkNumber() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

    url = "https://advokat.vipsite.kz/api/$_api/phone_feedback/";

    final response = await http.get(url);

    data = jsonDecode(response.body);

    sp.setString('phone_number', data);

//    setState(() {
//
//      data;
//
//    });

  }

  void getInfo() async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _number = sp.getString("phone_number");
  }
  int count;

  void getCount() async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    url = "https://advokat.vipsite.kz/api/$_api/answers/count";
    final response = await http.get(url);
    setState(() {
      count = jsonDecode(response.body)['count'];
    });

  }
  void checkUser() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

    print(_api);

    url = "https://advokat.vipsite.kz/api/$_api/about_you/user/user_data";

    final response = await http.get(url);

    user = jsonDecode(response.body);

    print(user);

    sp.setString('user_image', user["image_link"]);
    sp.setString('phone_number_user', user["phone_number"]);
    sp.setString('nameText', user['user_name']);
    sp.setString('emailText', user['user_email']);
    sp.setString('cityID', user['user_city']);

  }

  void test() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    print(sp.getKeys());
  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    checkUser();
    test();
    getCount();
    getLang();
   // checkNumber();
   // getInfo();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    final w = queryData.size.width;
    final h = queryData.size.height;

    getInfo();
    getLang();
    return Scaffold(
      drawer: NavDrawer(count: count,),

/*
      appBar: AppBar(

        backgroundColor: Colors.transparent,
        elevation: 0.0,

//        flexibleSpace: Container(
//          decoration: new BoxDecoration(
//            gradient: LinearGradient(
//                begin: FractionalOffset.topCenter,
//                end: FractionalOffset.bottomCenter,
//                colors: [
//                  Color(0xff4267b2),
//                  Color(0xff4267b2),
//                ],
//                tileMode: TileMode.repeated),
//          ),
//        ),

        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        title: Text("Личный адвокат",
          style: TextStyle(
              color: Colors.white
          ),
        ),
        //iconTheme: new IconThemeData(color: Colors.black),
        actions: <Widget>[
          FlatButton(
            child: Icon(Icons.share,
                color: Colors.white),
            onPressed: (){
              Share.share("Поделиться приложением, ссылка: url");
            },
          )
        ],
      ),


*/
//      body: MainMenuBody(),

    extendBody: false,

      body: GestureDetector(
        onTap: (){
          getCount();
        },
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage("assets/main.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(top: 0),
                              width: MediaQuery.of(context).size.width / 1.5,
                              padding: EdgeInsets.all(12) ,
                              decoration: BoxDecoration(
                                color: Color(0xff4267b2),
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Text(trans('request_a_consultation', _lang), textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16),),
                            ),
                            onTap: (){
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AskQuestion_Step1Screen()));
                            },
                          ),

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              width: MediaQuery.of(context).size.width / 1.5,
                              padding: EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                color: Color(0xff4267b2),
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Text(trans('order_documents', _lang), textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16),),
                            ),
                            onTap: () async {
                              print('hello');
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => orderDocumentScreen(title: trans('order_documents', _lang))));
                            },
                          ),
                          /*
                      InkWell(
                        child: Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width / 1.5,
                          padding: EdgeInsets.all(15) ,
                          decoration: BoxDecoration(
                            color: Color(0xff4267b2),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Text('Бесплатная консультация\n $_number', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16),),
                        ),
                        onTap: (){
                          UrlLauncher.launch("tel:$_number");
                        },
                      ), */
                        ],
                      )
                  ),),
                Positioned(
                  child: AppBar(

                    backgroundColor: Colors.transparent,
                    elevation: 0.0,

//        flexibleSpace: Container(
//          decoration: new BoxDecoration(
//            gradient: LinearGradient(
//                begin: FractionalOffset.topCenter,
//                end: FractionalOffset.bottomCenter,
//                colors: [
//                  Color(0xff4267b2),
//                  Color(0xff4267b2),
//                ],
//                tileMode: TileMode.repeated),
//          ),
//        ),

                    iconTheme: IconThemeData(
                      color: Colors.white, //change your color here
                    ),
                    title: Container(),
                    //iconTheme: new IconThemeData(color: Colors.black),
                    actions: <Widget>[
                      Container(
                          child: FlatButton(
                            child: Icon(Icons.share,
                                color: Colors.white),
                            onPressed: (){
                              Share.share("Поделиться приложением Ваш личный адвокат, ссылка: https://play.google.com/store/apps/details?id=kz.itgroup.personallawyer&hl=ru");
                            },
                          )
                      )
                    ],
                  ),
                ),
                Positioned(
                  right:w-50,
                  bottom:h-50,
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: Center(
                      child: Text('$count',style: TextStyle(color:Colors.white),),
                    ),
                  ),

                )
              ],
            )
          ],
        ),
      )

    );
  }




}