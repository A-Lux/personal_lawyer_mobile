import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:expandable/expandable.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:personallawyer/userProfil.dart';

import 'feedbackTopic.dart';

class QuestionPage1Screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return QuestionPage1ScreenState();
  }
}

class QuestionPage1ScreenState extends State<QuestionPage1Screen> {

  String data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text("Вопрос №1000", style: TextStyle(color: Colors.white)),
        ),

        body:  SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[


                        Container(
                          padding: EdgeInsets.only(top: 20, bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Text('Как снять с учета автомобиль в данном случае', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                              ),

                              Container(
                                padding: EdgeInsets.only(top: 10),
                                child: Text('г. Уральск', style: TextStyle(color: Colors.grey),),
                              ),

                              /* Container(
                                padding: EdgeInsets.only(top: 10),
                                child: Text('Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне.'),
                              ), */

                              Container(
                                padding: EdgeInsets.only(top: 10),
                                child: ExpandablePanel(
                                  header: Text( 'Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне.' + '...',
                                    maxLines: 10,
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                  expanded: Container(
                                    child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          'Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне. Добрый день. Подскажите пожалуйста. Мне необходимо снять с учета уже не существующий автомобиль. Зарегистрирован он в далекой деревне.',
                                          softWrap: true,
                                        )),
                                  ),
                                  tapHeaderToExpand: true,
                                  // hasIcon: true,
                                ),
                              ),

                              Container(
                                padding: EdgeInsets.only(top: 10),
                                child:  Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('10:44', style: TextStyle(color: Colors.grey),),
                                    Text('Развернуть', style: TextStyle(color: Colors.blue),)
                                  ],
                                ),
                              ),


                            ],
                          ),
                        ),

                      ],
                    ),
                  ),


                  Divider(height: 2, color: Colors.grey,),

                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10, right: 20, left: 20),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.receipt, color: Colors.blue),
                        Container(
                          padding: EdgeInsets.only(left: 5),
                          child: Text('Уточнить', style: TextStyle(color: Colors.blue),),
                        )
                      ],
                    ),
                  ),

                  Divider(height: 2, color: Colors.grey,),

                  Container(
                    color: Color(0xffe6e6e6),
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(top:15, bottom: 15, left: 20),
                    child: Text('Ответы юристов:' + ' 1', textAlign: TextAlign.left,),
                  ),

                  Divider(height: 2, color: Colors.grey,),

                  Container(
                    child: Column(
                      children: <Widget>[

                        Container(
                          //color: Colors.red,
                            padding: EdgeInsets.only(left: 20, top:15.0),
                            child:  Row(
                              children: <Widget>[
                                CircularProfileAvatar(
                                  data == null ? 'http://database.itgk.kz/img/rotov.jpg' : data,
                                  radius: 35,
                                  backgroundColor: Colors.transparent,
                                  //borderWidth: 3,
                                  //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
                                  borderColor: Colors.white,
                                  elevation: 5.0,
                                  //foregroundColor: Colors.white10.withOpacity(0.5),
                                  cacheImage: true,
                                  onTap: () {
                                    // getImage(context);
                                  },
                                  showInitialTextAbovePicture: true,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 15),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      InkWell(
                                        child: Container(
                                          child: Text('Ротов Алексей'),
                                        ),
                                        onTap: (){
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => UserProfilScreen()));
                                        },
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Text('Юрист, г. Тамбов', style: TextStyle(color: Colors.grey),),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            )
                        ),


                        Container(
                          padding: EdgeInsets.only(top: 15, left: 20, right: 20),
                          child: Text('Елена, добрый день! ДА в описание ситуации если первый вычет получали только по стоимости самой крватиры имеет право на основании. Супруг если ранее не использовал вычет может получить на кого оформлены договоры. Елена, добрый день! ДА в описание ситуации если первый вычет получали только по стоимости самой крватиры имеет право на основании. Супруг если ранее не использовал вычет может получить на кого оформлены договоры'),
                        ),

                        Container(
                          padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text('Вчера, 18:15', textAlign: TextAlign.left, style: TextStyle(color: Colors.grey),),
                          ),
                        ),

                        Divider(height: 2.0,),

                      ],
                    ),
                  ),

                  Container(
                    child: Column(
                      children: <Widget>[

                        Container(
                          //color: Colors.red,
                            padding: EdgeInsets.only(left: 20, top:15.0),
                            child:  Row(
                              children: <Widget>[
                                CircularProfileAvatar(
                                  data == null ? 'http://database.itgk.kz/img/rotov.jpg' : data,
                                  radius: 35,
                                  backgroundColor: Colors.transparent,
                                  //borderWidth: 3,
                                  //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
                                  borderColor: Colors.white,
                                  elevation: 5.0,
                                  //foregroundColor: Colors.white10.withOpacity(0.5),
                                  cacheImage: true,
                                  onTap: () {
                                    // getImage(context);
                                  },
                                  showInitialTextAbovePicture: true,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 15),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Text('Ротов Алексей'),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Text('Юрист, г. Тамбов', style: TextStyle(color: Colors.grey),),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            )
                        ),


                        Container(
                          padding: EdgeInsets.only(top: 15, left: 20, right: 20),
                          child: Text('Елена, добрый день! ДА в описание ситуации если первый вычет получали только по стоимости самой крватиры имеет право на основании. Супруг если ранее не использовал вычет может получить на кого оформлены договоры. Елена, добрый день! ДА в описание ситуации если первый вычет получали только по стоимости самой крватиры имеет право на основании. Супруг если ранее не использовал вычет может получить на кого оформлены договоры'),
                        ),

                        Container(
                          padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text('Вчера, 18:15', textAlign: TextAlign.left, style: TextStyle(color: Colors.grey),),
                          ),
                        ),

                        Divider(height: 2.0,),

                      ],
                    ),
                  ),



                ],
              ),
            )
        )
    );
  }
}