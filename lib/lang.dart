import 'package:shared_preferences/shared_preferences.dart';

Map<String, String>translate = new Map<String, String>();

  void setTrans() {

    // KAZ
    translate["ready_documents_kaz"] = 'Менің құжаттарым';
    translate["order_documents_kaz"] = 'Құжатқа тапсырыс беру';
    translate["request_a_consultation_kaz"] = 'Кеңес алуға тапсырыс беру';
    translate["my_questions_kaz"] = 'Менің сұрақтарым';
    translate["questions_on_the_project_kaz"] = 'Жобадағы сұрақтар';
    translate["settings_kaz"] = 'Параметрлер';
    translate["feedback_kaz"] = 'Кері байланыс';
    translate["tell_us_about_us_kaz"] = 'Біз туралы айтып беріңізші';
    translate["rate_the application_kaz"] = 'Қосымшасын бағалаңыз';
    translate["developed_by_kaz"] = 'Әзірленген ';
    translate["documents_info_kaz"] = 'Cіз әлі құжаттарға тапсырыс бермедіңіз.';
    translate["order_date_kaz"] = 'Тапсырыс күні: ';
    translate["status_kaz"] = 'Мәртебесі: ';
    translate["download_document_kaz"] = 'Құжатты жүктеу';
    translate["share_this_kaz"] = 'Бөлісу';
    translate["collapse_kaz"] = 'Жию';
    translate["expand_kaz"] = 'Жаю';
    translate["clarification_kaz"] = 'Нақтылау';
    translate["step1of3_kaz"] = '3-тің 1 қадамы';
    translate["documents_name_kaz"] = 'Құжаттың атауы';
    translate["field_to_fill_kaz"] = 'Толтыруға арналған өріс';
    translate["detailed_information_kaz"] = 'Толық ақпарат';
    translate["further_kaz"] = 'Бұдан әрі';
    translate["please_fill_in_the_fields_kaz"] = 'Өрістерді толтырыңыз!';
    translate["about_you_kaz"] = 'Сіз туралы';
    translate["step2of3_kaz"] = '3-тің 2 қадамы';
    translate["full_name_kaz"] = 'ТАӘ';
    translate["choose_a_city_kaz"] = 'Қаланы таңдаңыз';
    translate["search_kaz"] = 'Іздеу';
    translate["phone_kaz"] = 'Телефон';
    translate["info_text_kaz"] = 'Заңгер толығырақ ақпаратты нақтылау үшін сізбен телефон арқылы байланыса алады. Сіз үшін қоңырау тегін. Нөмір еш жерде жарияланбайды.';
    translate["document_type_kaz"] = 'Құжат түрі';
    translate["step3of3_kaz"] = '3-тің 3-қадамы';
    translate["what_do_you_want_to_order_kaz"] = 'Қандай тапсырыс бересіз: ';
    translate["total_kaz"] = 'Жиыны: ';
    translate["tenge_kaz"] = ' теңге';
    translate["to_pay_kaz"] = 'Төлем жүргізу';
    translate["payment_kaz"] = 'Ақы төлеу';
    translate["you_did_not_pay_the_bill_kaz"] = 'Сіз шот бойынша ақы төлемедіңіз';
    translate["your_application_has_not_been_published_kaz"] = 'Сіздің өтініміңіз жарияланбады';
    translate["after_payment_our_lawyers_will_prepare_a_document_kaz"] = 'Төлем жүргізілгеннен кейін, біздің заңгерлеріміз құжатты дайындайды';
    translate["cancel_the_application_kaz"] = 'Өтінімді болдырмау';
    translate["please_choose_your_city_kaz"] = 'Өз қалаңызды таңдаңыз!';
    translate["question_text_kaz"] = 'Сұрақ мәтіні: ';
    translate["question_title_kaz"] = 'Сұрақтың тақырыбы: ';
    translate["text_question_kaz"] = 'Сұрақтың мәтіні: ';
    translate["question_type_kaz"] = 'Сұрақтың түрі: ';
    translate["your_question_is_not_published_kaz"] = 'Сіздің сұрағыңыз жарияланбаған';
    translate["info_text2_kaz"] = '– Төлем жүргізілгеннен кейін,\nСіздің сұрақтарыңызға заңгерлер\n жауап бере алады';
    translate["successful_post_kaz"] = 'Табысты жариялау';
    translate["your_question_has_been_successfully_posted_kaz"] = 'Сіздің сұрақтарыңыз сәтті жарияланды';
    translate["info_text3_kaz"] = 'Бір сағат ішінде оған\nзаңгерлердің жауаптары түседі';
    translate["go_to_main_page_kaz"] = 'Басты бетке өту';
    translate["your_application_has_been_successfully_published_kaz"] = 'Сіздің өтініміңіз сәтті жарияланды';
    translate["info_text4_kaz"] = 'Жақын арада біздің\nзаңгерлеріміз құжатты дайындайды';
    translate["issue_price_kaz"] = 'Сұрақ бағасы: ';
    translate["of_responses_kaz"] = ' жауаптар';
    translate["question_kaz"] = 'Сұрақ №: ';
    translate["clarify_kaz"] = 'Нақтылау';
    translate["lawyer_profile_kaz"] = 'Заңгер профилі';
    translate["reviews_kaz"] = 'Пікірлер: ';
    translate["positive_evaluation_kaz"] = 'Оң баға: ';
    translate["write_a_review_kaz"] = 'Пікір жазу';
    translate["education_kaz"] = 'Білім: ';
    translate["courses_kaz"] = 'Курстар: ';
    translate["reviews2_kaz"] = 'Пікірлер: ';
    translate["feedback3_kaz"] = 'Пікір';
    translate["leave_feedback_kaz"] = 'Пікір қалдыру';
    translate["fill_in_all_required_fields_kaz"] = 'Барлық міндетті өрістерді толтырыңыз!';
    translate["adding_clarification_kaz"] = 'Нақтылауды қосу';
    translate["submit_kaz"] = 'Жіберу';
    translate["notification_kaz"] = 'Хабарлама';
    translate["feedback2_kaz"] = 'Кері байланыс';
    translate["theme_of_appeal_kaz"] = 'Өтініш тақырыбы';
    translate["info_text5_kaz"] = 'Біздің қолданбаны қолданғаныңыз\nүшін алғыс айтамыз.  Достарыңызға\nСіздің жеке адвокатыңыз сервисі\nтуралы айта жүріңіз.';
    translate["i_do_not_like_kaz"] = 'Ұнамайды';
    translate["info_text6_kaz"] = 'Қолданба ыңғайсыз, ұзақ жүктеледі, қажеттіні  табуға көмектеспейді.';
    translate["like_kaz"] = 'Ұнайды';
    translate["info_text6_kaz"] = 'Қолданба ұнайды, оларды пайдалану ыңғайлы.';
    translate["you_have_not_asked_another_question_kaz"] = 'Сіз  әлі сұрақ қоймадыңыз.';
    translate["replies_from_lawyers_have_not_yet_been_received_kaz"] = 'Заңгерлердің жауабы әлі түскен жоқ';
    translate["no_reviews_kaz"] = 'Пікірлер жоқ';
    translate["your_post_has_been_successfully_posted2_kaz"] = 'Сіздің жазбаңыз сәтті жарияланды.';

    // RUS
    translate["ready_documents_rus"] = 'Мои документы';
    translate["order_documents_rus"] = 'Заказать документы';
    translate["request_a_consultation_rus"] = 'Получить консультацию';
    translate["my_questions_rus"] = 'Мои вопросы';
    translate["questions_on_the_project_rus"] = 'Вопросы на проекте';
    translate["settings_rus"] = 'Настройки';
    translate["feedback_rus"] = 'Обратная связь';
    translate["tell_us_about_us_rus"] = 'Расскажите о нас';
    translate["rate_the application_rus"] = 'Оцените приложение';
    translate["developed_by_rus"] = 'Разработано в ';
    translate["documents_info_rus"] = 'Вы еще не заказывали документов.';
    translate["order_date_rus"] = 'Дата заказа: ';
    translate["status_rus"] = 'Статус:';
    translate["download_document_rus"] = 'Скачать документ';
    translate["share_this_rus"] = 'Поделиться';
    translate["collapse_rus"] = 'Свернуть';
    translate["expand_rus"] = 'Развернуть';
    translate["clarification_rus"] = 'Уточнение';
    translate["step1of3_rus"] = 'шаг 1 из 3';
    translate["documents_name_rus"] = 'Название документа';
    translate["field_to_fill_rus"] = 'Поле для заполнения';
    translate["detailed_information_rus"] = 'Подробная информация';
    translate["further_rus"] = 'Далее';
    translate["please_fill_in_the_fields_rus"] = 'Пожалуйста заполните поля!';
    translate["about_you_rus"] = 'О вас';
    translate["step2of3_rus"] = 'шаг 2 из 3';
    translate["full_name_rus"] = 'ФИО';
    translate["choose_a_city_rus"] = 'Выберите город';
    translate["search_rus"] = 'Поиск';
    translate["phone_rus"] = 'Телефон';
    translate["info_text_rus"] = 'По телефону с вами может связаться юрист для уточнения подробностей. Для вас звонок бесплатный. Номер нигде не публикуется.';
    translate["document_type_rus"] = 'Тип документа';
    translate["step3of3_rus"] = 'шаг 3 из 3';
    translate["what_do_you_want_to_order_rus"] = 'Что вы хотите заказать:';
    translate["total_rus"] = 'Итого:';
    translate["tenge_rus"] = ' тенге';
    translate["to_pay_rus"] = 'Оплатить';
    translate["payment_rus"] = 'Оплата';
    translate["you_did_not_pay_the_bill_rus"] = 'Вы не оплатили счет';
    translate["your_application_has_not_been_published_rus"] = 'Ваша заявка не опубликована';
    translate["after_payment_our_lawyers_will_prepare_a_document_rus"] = 'После оплаты, наши юристы\n подготовят документ';
    translate["cancel_the_application_rus"] = 'Отменить заявку';
    translate["please_choose_your_city_rus"] = 'Пожалуйста выберите ваш город!';
    translate["question_text_rus"] = 'Текст вопроса';
    translate["question_title_rus"] = 'Заголовок вопроса:';
    translate["text_question_rus"] = 'Текс вопроса:';
    translate["question_type_rus"] = 'Тип вопроса';
    translate["your_question_is_not_published_rus"] = 'Ваш вопрос не опубликован';
    translate["info_text2_rus"] = 'После оплаты, на ваш вопрос\n смогут ответить юристы';
    translate["successful_post_rus"] = 'Успешная публикация';
    translate["your_question_has_been_successfully_posted_rus"] = 'Ваш вопрос успешно опубликован';
    translate["info_text3_rus"] = 'В течение часа на него поступят\nответы юристов';
    translate["go_to_main_page_rus"] = 'Перейти на главную';
    translate["your_application_has_been_successfully_published_rus"] = 'Ваша заявка успешно опубликована';
    translate["info_text4_kaz"] = 'В ближайшее время наши юристы\nподготовят документ';
    translate["issue_price_rus"] = 'Цена вопроса: ';
    translate["of_responses_rus"] = ' ответов';
    translate["question_rus"] = 'Вопрос №: ';
    translate["clarify_rus"] = 'Уточнить';
    translate["lawyer_profile_rus"] = 'Профиль юриста';
    translate["reviews_rus"] = 'Отзывов: ';
    translate["positive_evaluation_rus"] = 'Положительной оценки: ';
    translate["write_a_review_rus"] = 'Написать отзыв';
    translate["education_rus"] = 'Образование:';
    translate["courses_rus"] = 'Курсы:';
    translate["reviews2_rus"] = 'Отзывы:';
    translate["feedback3_rus"] = 'Отзыв';
    translate["leave_feedback_rus"] = 'Оставить отзыв';
    translate["fill_in_all_required_fields_rus"] = 'Заполните все обязательные поля!';
    translate["adding_clarification_rus"] = 'Добавление уточнения';
    translate["submit_rus"] = 'Отправить';
    translate["notification_rus"] = 'Уведомление';
    translate["feedback2_rus"] = 'Обратная связь';
    translate["theme_of_appeal_rus"] = 'Тема обращения';
    translate["info_text5_rus"] = 'Благодарим за использование нашего\n приложения. Расскажите своим\n друзьям о сервисе Ваш личный адвокат.';
    translate["i_do_not_like_rus"] = 'Не нравится';
    translate["info_text6_rus"] = 'Приложение неудобное, долго загружается не помогает найти нужное.';
    translate["like_rus"] = 'Нравится';
    translate["info_text6_rus"] = 'Приложение нравится, им приятно пользоваться.';
    translate["you_have_not_asked_another_question_rus"] = 'Вы не задавали еще вопрос.';
    translate["replies_from_lawyers_have_not_yet_been_received_rus"] = 'Ответы от юристов еще не поступили';
    translate["no_reviews_rus"] = 'Отзывы отсутствуют';
    translate["your_post_has_been_successfully_posted2_rus"] = 'Ваша запись успешно опубликована.';



  }

  String _lang;


 void getLang() async {
   SharedPreferences prefs = await SharedPreferences.getInstance();
   String _lang = prefs.getString("language");
 }

  String trans(String key, var lang) {
  //  getLang();

    if (lang == null || lang == "") {
      lang = "rus";
    }

 //lang = _lang;

    setTrans();
    var temp = translate[key +'_' + lang.toString()];
    if(temp == null) {
      print('Алеша, ключа `' + key + '` с языком `' + lang.toString() + '` не существует!');
      temp = key+'_'+lang.toString().toUpperCase();
    }
    return temp;
  }