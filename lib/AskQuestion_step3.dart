import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:personallawyer/finishedDocumentsPage.dart';
import 'package:personallawyer/paybox/PayCard.dart';
import 'package:personallawyer/paybox/QuestionPay.dart';
import 'package:personallawyer/paybox/url_launcher.dart';
import 'package:personallawyer/seccessfulPublication.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'lang.dart';

class AskQuestion_Step3Screen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return AskQuestion_Step3ScreenState();
  }
}


class AskQuestion_Step3ScreenState extends State<AskQuestion_Step3Screen> {

  var data;

  String header_question;
  String text_question;
  String user_name;
  String phone_number;
  String user_city;
  String cityID;
  String user_email;
  String _api;
  var isLoading = true;
  String description = "Оплата за вопрос";
  String _lang;
  String url;
  var services;
  bool chooseCard = false;
  List<Widget>myList = new List<Widget>();
  ListView list;
  var _price = 0;
  List<dynamic> cards = [];

  bool isChecked = false;
  List<bool> checkboxes = new List<bool>();
//  List<int> prices = new List<int>();
  Map prices = new Map();

  Map<String,dynamic> card;

  void getInfo() async {

    SharedPreferences sp = await SharedPreferences.getInstance();

    header_question = sp.getString("titleText");
    text_question = sp.getString('descText');
    user_name = sp.getString('nameText');
    phone_number = sp.getString('phone_number_user');
    user_city = sp.getString('cityText');
    user_email = sp.getString('emailText');
    cityID = sp.getString('cityID');

    print('cityID: ' + cityID);

  }

  void sendRequest() async {

    print(_api);
    print(header_question);
    print(text_question);
    print(user_name);
    print(phone_number);
    print(cityID);
    print(user_email);
    print(_price);

    print(prices);

    final response = await http.post('https://advokat.vipsite.kz/api/question/add_question',
    body: {
      'api_token': _api,
      'header_question': header_question,
      'text_question': text_question,
      'user_name': user_name,
      'phone_number': phone_number,
      'user_city': cityID,
      'user_email': user_email,
      'issue_price': jsonEncode(prices),
    });

    print(_api);
    print(header_question);
    print(text_question);
    print(user_name);
    print(phone_number);
    print(cityID);
    print(user_email);
    print(_price);

    print(jsonEncode(_price));
    if(response.statusCode == 200) {

//      Navigator.push(
//          context,
//          MaterialPageRoute(
//              builder: (context) => seccessfulPublicationScreen()));

    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString('id', response.body);

    Pay();

//      Fluttertoast.showToast(
//        msg: jsonDecode(response.body),
//        timeInSecForIos: 3,
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.CENTER,
//        textColor: Colors.white,
//        backgroundColor: Colors.green,
//      );
    } else {
      print(jsonEncode(prices));
      print(_api);
      print(response.body);
    }
  }

  void getServices() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "https://advokat.vipsite.kz/api/$_api/question/valuable/questions/$_lang";

    print(url);

    final response = await http.get(url);
    print(response.body);
    services = jsonDecode(response.body);

    setState(() {
      services;
      isLoading = false;
    });


//    for (int i = 0; i < services.length; i++) {
//
//    checkboxes.add(false);
//
//      myList.add(
//          new Column(
//            children: <Widget>[
//              Card(
//                child: new CheckboxListTile(
//                  title: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
//                      Text(services[i]["are_valuable"].toString() + ' тенге', style: TextStyle(color: Colors.green),),
//                    ],
//                  ),
//                 // value: checkboxes[i],
//                  value: checkboxes[i],
//                  onChanged: (value) {
//                    if(checkboxes[i] == false){
//                      prices["checkbox_"+services[i]['id'].toString()] = services[i]['id'];
//                      setState(() {
//                        _price += services[i]['are_valuable'];
//                        checkboxes[i] = true;
//                        print(prices.toString());
//                      });
//                    } else{
//                      prices.remove('checkbox_'+services[i]['id'].toString());
//                      setState(() {
//                        _price -= services[i]['are_valuable'];
//                        checkboxes[i] = false;
//                        print(prices.toString());
//                      });
//                    }
//                  },
//                ),
//              ),

//             new CheckboxListTile(
//                  title: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
//                      Text(services[i]["are_valuable"].toString() + ' тенге', style: TextStyle(color: Colors.green),),
//                    ],
//                  ),
////                  value: false,
////                  selected: checkboxes[i],
//                  value: services[i]['isCheck'],
//                  onChanged: (value) {
//                    setState(() {
//                      if(services[i]['isCheck'] == false){
//                        prices["checkbox_"+services[i]['id'].toString()] = services[i]['id'];
//                        _price += services[i]['are_valuable'];
//                        services[i]['isCheck'] = true;
////                        checkboxes[i] = true;
//                      } else {
//                        prices.remove('checkbox_'+services[i]['id'].toString());
//                        _price -= services[i]['are_valuable'];
//                        services[i]['isCheck'] = false;
////                        checkboxes[i] = false;
//                      }
//                    });
//                  },
//                ),
//
//            ],
//          )
//      );
//    }
  }

  void Pay() async {

    final SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString('apikey');
    var id = sp.getString('id');

    print(id);

    url = "https://advokat.vipsite.kz/api/$_api/payment/api_paybox_payment/$id/questions/";

    print(url);

    final response = await http.get(url);

    data = jsonDecode(response.body);
    print(data);

    setState(() {
      data;


      Navigator.push(context, MaterialPageRoute(builder: (context) => LaunchUrl(url: data,)));

    });

  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void linkCard()async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    url = "https://advokat.vipsite.kz/api/$_api/payment/link-card";
    print(url);
    final response =await http.post(url);
    final body  = jsonDecode(response.body);
    print(body);
    Navigator.push(context,MaterialPageRoute(builder: (context)=>LaunchUrl(url:body['response']['pg_redirect_url'])));


  }

  void getCards()async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    url = "https://advokat.vipsite.kz/api/$_api/payment/cards";
    final response = await http.get(url);
    final body =  jsonDecode(response.body);

    setState(() {
      cards = body['cards'];
      print(cards);
    });

  }
  void initState() {
    super.initState();

    getLang();
    getInfo();

    getServices();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData (
          color: Colors.white,
        ),
        title: Text(trans('question_type', _lang), style: TextStyle(color: Colors.white)),

        actions: <Widget>[
          FlatButton(
              child: Container(
                child: Text(trans('step3of3', _lang), style: TextStyle(color: Colors.white),),
              )
          )
        ],
      ),

      body:  isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          :  SingleChildScrollView(
        child: Container (
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 10),
                child: Text(trans('what_do_you_want_to_order', _lang), style: TextStyle(fontSize: 17), textAlign: TextAlign.left,),
              ),


              Column(
                children: myList,
              ),



              Container(
                padding: EdgeInsets.only(right: 15, left: 15),
                height: 80,
                child: ListView.builder(
                    itemCount: services.length,
                    itemBuilder: (context, i){
//                      prices.add(0);
//                      checkboxes.add(false);
                      return new Card(
                        child: CheckboxListTile(
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
                              ],
                            ),
                            subtitle: Text(services[i]["are_valuable"].toString() + trans('tenge', _lang), style: TextStyle(color: Colors.black),),
                            value: services[i]['isCheck'],
                            onChanged: (value) {
                              setState(() {
                                if(services[i]['isCheck'] == false){
                                  prices["checkbox_"+services[i]['id'].toString()] = services[i]['id'];
                                  _price += services[i]['are_valuable'];
                                  services[i]['isCheck'] = true;
                                  //                        checkboxes[i] = true;
                                } else {
                                  prices.remove('checkbox_'+services[i]['id'].toString());
                                  _price -= services[i]['are_valuable'];
                                  services[i]['isCheck'] = false;
                                  //                        checkboxes[i] = false;
                                }
                              });
                            }
                        ),
                      );
                }),
              ),
              GestureDetector(

                child: Column(
                  children: [
                    GestureDetector(
                      onTap: (){
                        chooseCard = !chooseCard;
                        getCards();

                      },
                      child: Container(
                        padding: const EdgeInsets.only(top:20,left:20,right:20),
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black,),
                            borderRadius: BorderRadius.circular(10)


                          ),
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Center(
                                child: Text(card != null ? card['card_hash'] :'Выбрать карту'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    chooseCard == true ? Container(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10),

                      child: Container(
                        height: 150,
                        child: ListView.builder(
                            itemCount: cards.length,
                            itemBuilder: (context,index){
                              return Column(
                                children: [
                                  GestureDetector(
                                    onTap:(){
                                      setState(() {
                                        chooseCard = false;
                                        card = cards[index];
                                        print(card);
                                      });

                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          border: index != 0 ? Border(top: BorderSide(color: Colors.black )) : Border(top:BorderSide(color:Colors.white)),
                                      ) ,

                                      padding: const EdgeInsets.symmetric(vertical: 15,),
                                      child: Center(
                                        child: Text('${cards[index]['card_hash']}',style: TextStyle(fontSize: 15),),

                                      ),
                                    ),
                                  ),
                                  index == cards.length-1 ?
                                  GestureDetector(
                                    onTap:()async{
                                      linkCard();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: cards.length != 0 ? Border(top: BorderSide(color: Colors.black )) : Border.all(color:Colors.white),
                                      ) ,

                                      padding: const EdgeInsets.symmetric(vertical: 15,),
                                      child: Center(
                                        child: Text('Добавить карту',style: TextStyle(fontSize: 15),),

                                      ),
                                    ),
                                  ): SizedBox.shrink()

                                ],
                              );
                            }
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)
                        ),
                      ),
                    ) : SizedBox.shrink()
                  ],
                ),
              ),




              Container(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 10),
                  child: Row(
                    children: <Widget>[
                      Text(trans('total', _lang), style: TextStyle( fontSize: 17), textAlign: TextAlign.left,),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text('${_price}' + trans('tenge', _lang), style: TextStyle(fontSize: 17), textAlign: TextAlign.left,),
                      )
                    ],
                  )
              ),

//                  Container(
//                    padding: EdgeInsets.only(top: 20, left: 15, right: 15),
//                    child: Column(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//                        InkWell(
//                          child: Container(
//                              width: 200,
//                              padding: EdgeInsets.all(10),
//                              decoration: BoxDecoration(
//                                color: Colors.green,
//                                borderRadius: BorderRadius.circular(10),
//                              ),
//                              child: Align(
//                                alignment: Alignment.center,
//                                child: Text('Оплатить', style: TextStyle(color: Colors.white),),
//                              )
//                          ),
//                          onTap: (){
//
//                          },
//                        ),
//                      ],
//                    ),
//                  )

            ],
          ),
        ),
      ),

      bottomNavigationBar: Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: FlatButton(
            onPressed: () {
           if(_price > 0){
             if(card != null){

               sendCardRequest();
               print('lol');
             }else{
               sendRequest();
               print(_price);
             }
           }
//              Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (context) => QuestionPay(price:_price)));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.account_balance_wallet,
                    color: Colors.white
                ),
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                      trans('to_pay', _lang),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15
                      )
                  ),
                )
              ],
            )
        ),
      ),

    );


  }

  void sendCardRequest() async{
    final response = await http.post('https://advokat.vipsite.kz/api/question/add_question',
        body: {
          'api_token': _api,
          'header_question': header_question,
          'text_question': text_question,
          'user_name': user_name,
          'phone_number': phone_number,
          'user_city': cityID,
          'user_email': user_email,
          'issue_price': jsonEncode(prices),
        });


    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString('id', response.body);

    if(response.statusCode == 200) {
      final url = 'https://advokat.vipsite.kz/api/$_api/payment/pay-by-card';
      var id = sp.getString('id');
      print(id);
      final payResponse = await http.post(url,body: {
        'user_card_id' : card['id'].toString(),
        'pay_id' : id,
        'description' : 'questions'
      });
      final mainPayment = jsonDecode(payResponse.body)['payment'];
      Navigator.push(context,MaterialPageRoute(builder: (context)=> PayCard(htmlData:mainPayment,isDocumentSale: false,)));

    }


  }
}