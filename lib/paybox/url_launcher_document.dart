import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:personallawyer/seccessfulPublication.dart';
import 'package:personallawyer/seccessfulPublicationDocument.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import '../lang.dart';

class LaunchUrlDocument extends StatefulWidget{

  final String url;


  LaunchUrlDocument({Key key, @required this.url}): super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LaunchUrlDocumentState();
  }

}



class LaunchUrlDocumentState extends State<LaunchUrlDocument> {

  final fwvp = new FlutterWebviewPlugin();
  bool done  = false;
  bool call = false;
  String _lang;
  String _num;
  String _call = "";

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    startCounter();
    super.initState();
    getLang();
  }


  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData (
          color: Colors.white,
        ),
        title: Text(trans('payment', _lang), style: TextStyle(color: Colors.white)),

        leading: InkWell(
          child: Icon(Icons.arrow_back_ios),
          onTap: (){
            Navigator.pushReplacement( context,
                MaterialPageRoute(
                    builder: (context) => seccessfulPublicationDocumentScreen()));
          },
        ),

      ),

      body: Column(
          children: <Widget>[
          Container(
            child: done
                ? Container()
                : LinearProgressIndicator(),
          ),
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height - 100,
                child: WebviewScaffold(
                  url: widget.url,
                  withZoom: true,
                )
            ),
          ],
        ),
    );
  }

  void startCounter() {
    Future.delayed(const Duration(seconds: 4), () {
      setState((){
        done = true;
      });
    });
  }
}