import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'lang.dart';
import 'QuestionPage.dart';
import 'myQuestionPage.dart';

class MyQuestionScreen extends StatefulWidget {

  var title;

  MyQuestionScreen({Key key, @required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyQuestionScreenState();
  }
}

class MyQuestionScreenState extends State<MyQuestionScreen> {

  String _api;

  List<Posts> _listMap = [];
  var loading = false;

  var dataD;
  var data;
  String _lang;
  var isLoading = true;
  ScrollController _scrollController;
  var postsCount = 0;
  var contentIsDone = false;

  var enableNullText = false;
  var stopSpam = false;

  void fetchData() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    if (mounted) {
      setState(() {
        loading = true;
      });
    }

    final response = await http.get("https://advokat.vipsite.kz/api/$_api/question/user_question/$_lang");
    print(response.statusCode );

    dataD = jsonDecode(response.body);

    if (response.statusCode == 200 && jsonDecode(response.body) != 'null') {

        data = jsonDecode(response.body);

        if (mounted) {
          setState(
                  () {
                for (Map i in data) {
                  _listMap.add(Posts.formJson(i));
                  loading = false;
                  isLoading = false;
                }
              } );
        }

      } else {
        loading = false;

//        _scrollController..removeListener(_scrollListener );
      //  contentIsDone = true;
      }

    setState(() {
      dataD;
    });
  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  @override
  void initState(){
//    _scrollController = new ScrollController()..addListener(_scrollListener);
    fetchData();
    getLang();
    super.initState();
  }

  @override
  void dispose() {
//    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(widget.title, style: TextStyle(color: Colors.white)),
        ),

      /*  body: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[

                  ListTile(
                    title: Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                          Container(
                            padding: EdgeInsets.only(top: 5, bottom: 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                      Text(' 0 ответов', style: TextStyle(fontSize: 14),),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Text('Сегодня, 11:22', style: TextStyle(color: Colors.grey, fontSize: 14),),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  Divider(height: 2.0,),

                  ListTile(
                      title: Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                            Container(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                        Text(' 1 ответов', style: TextStyle(fontSize: 14),),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Text('Сегодня, 10:40', style: TextStyle(color: Colors.grey, fontSize: 14),),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      trailing: Container(
                        height: MediaQuery.of(context).size.height / 2,
                        child: Icon(Icons.arrow_forward_ios),
                      )
                  ),
                  Divider(height: 2.0,),

                  ListTile(
                      title: Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child:  Text('Цена вопроса: 1500 тг', style: TextStyle(fontSize: 15, color: Colors.green),),
                            ),
                            Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                            Container(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                        Text(' 3 ответов', style: TextStyle(fontSize: 14),),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Text('Вчера, 12:02', style: TextStyle(color: Colors.grey, fontSize: 14),),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      trailing: Container(
                        height: MediaQuery.of(context).size.height / 2,
                        child: Icon(Icons.arrow_forward_ios),
                      )
                  ),
                  Divider(height: 2.0,),



                ],
              )
          ),
        ) */

 body:isLoading
     ? Center(
     child: CircularProgressIndicator())
     :
 dataD == 'null' ? Container(
       width: MediaQuery.of(context).size.width,
       padding: EdgeInsets.only(top: 15),
       child: Text(trans('you_have_not_asked_another_question', _lang), textAlign: TextAlign.center,),
     ) :
     Container(
      padding: EdgeInsets.only(top:10),
     child: ListView.builder(
//    controller: _scrollController,
    itemCount: _listMap.length,
        itemBuilder: (context, i) {
      final a = _listMap[i];
      return Column(
        children: <Widget>[
          ListTile(
            title: Container(
              padding: EdgeInsets.only(top: 5, bottom: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  a.issue_price == 'null' ? Container() :
                  Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Text(trans('issue_price', _lang) + a.issue_price, style: TextStyle(fontSize: 15, color: Colors.green),),
                  ),
                  Text(a.header_question, style: TextStyle(fontWeight: FontWeight.bold),),
                  Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                              Text(a.new_answers_count + trans('of_responses', _lang), style: TextStyle(fontSize: 14),),
                            ],
                          ),
                        ),
                        Container(
                          child: Text(a.created, style: TextStyle(color: Colors.grey, fontSize: 14),),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            onTap:() {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => myQuestionPageScreen(id_question: a.id,)));
            },
            trailing: Icon(Icons.arrow_forward_ios),
          ),

          Divider(
            height: 0.2,
          )
        ], );
    })),

    );
  }

//  void _scrollListener() {
//
//    if(_scrollController.position.extentAfter < 100 && contentIsDone == false){
//
//      setState(() {
//        isLoading = false;
//        fetchData();
//        postsCount += 20;
//        stopSpam = true;
//        // await
//      });
//
//    }
//  }
}


class Posts {
  final String id;
  final String header_question;
  final String city_question;
  final String text_question;
  final String mobile_users_id;
  final String status_question;
  final String days_of_waiting;
  final String created;
  final String new_answers_count;
  final String issue_price;

  Posts({this.id, this.header_question, this.city_question, this.text_question, this.mobile_users_id, this.status_question, this.days_of_waiting, this.created, this.new_answers_count, this.issue_price});
  factory Posts.formJson(Map <String, dynamic> json){
    print(json.toString());
    return new Posts(
      id: json['id'].toString(),
      header_question: json['header_question'].toString(),
      city_question: json['city_question'].toString(),
      text_question: json['text_question'].toString(),
      mobile_users_id: json['mobile_users_id'].toString(),
      status_question: json['status_question'].toString(),
      days_of_waiting: json['days_of_waiting'].toString(),
      created: json['created'].toString(),
      new_answers_count: json['new_answers_count'].toString(),
      issue_price: json['issue_price'].toString(),
    );
  }
}
