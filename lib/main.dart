import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:personallawyer/main-star.dart';
import 'package:personallawyer/main-start-smscode.dart';
import 'PageView.dart';
import 'login.dart';
import 'mainMenuBody.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:upgrader/upgrader.dart';

import 'notifications.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Personal Lawyer',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Color(0xff4267b2),
      ),
      home: MyHomePage(title: 'Personal Lawyer'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController _phoneC = new MaskedTextController(mask: '+7(000)000-00-00');

  double setTop() {
    if(MediaQuery.of(context).size.width*MediaQuery.of(context).devicePixelRatio > 700){
      return 50;
    } else {
      return 65;
    }
  }

  var bloc = NotificationBloc();

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    if (_lang == null || _lang == "") {
      _lang = "rus";
    }

    await prefs.setString("language", _lang);

    print(_lang);
  }

  @override
  void initState() {
    getLang();
    super.initState();
    bloc.initOneSignal();
  }


  @override
  Widget build(BuildContext context) {
    getUserData();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return UpgradeAlert(
      dialogStyle: UpgradeDialogStyle.cupertino,

      child: Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/fon.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
        body: Container(
          child: Column(
           // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: setTop(), left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Text("Личный адвокат", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white, fontFamily: 'MyriadProRegular'),),
                    ),



                Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[

                         /*

                          Container(
                            child: FlatButton(
                              child: Text('Войти', style: TextStyle(fontSize: 18, color: Colors.white, fontFamily: 'MyriadProRegular'),),
                              onPressed: () {
                                Navigator.push(context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginScreen()));
                              }
                            ),
                          ),
                          Icon(Icons.arrow_forward_ios, color: Colors.white,)


                          */

                        ],
                      ),
                    )

                  ],
                ),
              ),



              Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/3.0, left: 16.0, right: 16.0),
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[

                      TextField(
                        onTap: (){
                         if(_phoneC.text.isEmpty){
                            _phoneC.text = "+7";
                         }
                        },
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white,
                        ),
                        controller: _phoneC,
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                            hintText: "+7(***)***-**-**",
                            hintStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            ),

                            //                           border: InputBorder.none,
                            //labelText: _phone,
                            contentPadding: EdgeInsets.all(7.0),
                            labelStyle: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontSize: 18.0,
                            )),
                      ),
                      Builder(
                        builder: (context) => Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                padding:
                                EdgeInsets.only(top: 15, left: 32, right: 32),
                                //width: MediaQuery.of(context).size.width,
                                child: RaisedButton(
                                  color: Color(0xff4267b2),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(35),
                                  ),
                                  onPressed: () {
                                    checkUser(context);
                                  },
                                  child: Text(
                                    'Получить SMS-код',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'MyriadProRegular',
                                      fontSize: 15.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),

                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        // margin: EdgeInsets.only(bottom: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Builder(
                              builder: (context) => InkWell(
                                child:  Text('У меня уже есть SMS-код',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'MyriadProRegular',
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                onTap: () {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => MainStartSMSCodeScreen()));
                                },
                              ),
                            ),
                          ],
                        ),
                      ),




                    ],
                  )
              ),


          /*   Align(
               alignment: Alignment.centerLeft,
               child: Container(
                 padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/3.2, left: 20),
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[

                     Text('ЕСТЬ ВОПРОС', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold, color: Colors.white, fontFamily: 'MyriadProRegular'),),
                     Text('К ЮРИСТУ?', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold, color: Colors.white, fontFamily: 'MyriadProRegular'), textAlign: TextAlign.left,),
                     Container(
                       padding: EdgeInsets.only(top: 10, right: 20),
                       child: Text('Задайте его в приложении или позвоните на бесплатную горячую линию', style: TextStyle(color: Colors.white, fontSize: 15, fontFamily: 'MyriadProRegular')),
                     )

                   ],
                 ),
               )
             ),*/


         /*  Expanded(
             child:  Container(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                     RaisedButton(
                       child: Text("Задайте вопрос юристу"),
                     ),
                     RaisedButton(
                       child: Text("Позвонить"),
                     )
                   ],
                 )
             ),
           ) */


            ],
          ),
        ),

        /*

        bottomNavigationBar: Container(
          padding: EdgeInsets.only(bottom: 30,right: 25, left: 25),
          child: Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: <Widget>[

             InkWell(
               child: Container(
                 decoration: new BoxDecoration(
                   color: Color(0xffed5e68),
                   borderRadius: BorderRadius.vertical(
                       top: Radius.circular(10),
                       bottom: Radius.circular(10)
                   ),
                 ),
                 padding: EdgeInsets.all(10),
                 child: Row(
                   children: <Widget>[
                     Container(
                       child: Icon(Icons.chat_bubble, size: 35, color: Colors.white,),
                       padding: EdgeInsets.only(right: 10),
                     ),
                     Text('Заказать услугу', style: TextStyle(color: Colors.white, fontFamily: 'MyriadProRegular'),),
                   ],
                 ),
               ),
               onTap: (){

               },
             ),



             InkWell(
                 child: Container(
                   decoration: new BoxDecoration(
                     color: Color(0xff4267b2),
                     borderRadius: BorderRadius.vertical(
                         top: Radius.circular(10),
                         bottom: Radius.circular(10)
                     ),
                   ),
                   padding: EdgeInsets.all(10),
                   child: Row(
                     children: <Widget>[
                       Container(
                         child: Icon(Icons.call, size: 35, color: Colors.white,),
                         padding: EdgeInsets.only(right: 10),
                       ),
                       Text('Позвонить \n8(800)450-95-44   ', style: TextStyle(color: Colors.white, fontFamily: 'MyriadProRegular'),),
                     ],
                   ),
                 ),
               onTap: (){

               },
             ),


           ],

          ),

        )

              */

      )),
    );
  }



  Future checkUser(BuildContext context) async{

    if(_phoneC.text.isNotEmpty){

      var url = "https://advokat.vipsite.kz/api/login/send";

      final response = await http.post(url, body: {"phone": _phoneC.text});

      SetUser();


      print(response.statusCode);
      print(response.body);

      if (response.statusCode == 200) {

        Route route = MaterialPageRoute(builder: (context) => MainStartScreen());
        Navigator.pushReplacement(context, route);

      } else {
        Fluttertoast.showToast(
          msg: "Произошла ошибка",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
        );
        throw Exception('Failed to load files');
      }
    } else {
      Fluttertoast.showToast(
        msg: "Введите номер телефона",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }


  }


  void SetUser() async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String phoneI = _phoneC.text;
    await prefs.setString("phoneI", phoneI);

  }


  Future getUserData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool("logged") != null && prefs.getBool("logged")) {
      Route route = MaterialPageRoute(builder: (context) => PagerScreen());
      Navigator.pushReplacement(context, route);
    }
  }



}


