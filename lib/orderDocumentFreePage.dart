import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personallawyer/pravoved_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:share/share.dart';

class orderDocumentFreePageScreen extends StatefulWidget {

  var id;
  var title;

  orderDocumentFreePageScreen({Key key, @required this.id, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return orderDocumentFreePageScreenState();
  }
}

class orderDocumentFreePageScreenState extends State<orderDocumentFreePageScreen> {

  var data;
  bool _text = false;
  var url;
  var documents;
  int dataq;
  int dataqq = 351;
  var isLoading = true;
  var _number;

  void getDocumentsInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _api = prefs.getString("apikey");

    url = "https://advokat.vipsite.kz/api/$_api/document/show_free_document/${widget.id}";

    final response = await http.get(url);

    print(response.body);


    documents = jsonDecode(response.body);

    dataq = documents["doc_description"].length;

    print(documents);

    setState(() {
      documents;
      isLoading = false;
    });

  }

  void getInfo() async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _number = sp.getString("phone_number");
  }

  void initState() {
    getDocumentsInfo();
    getInfo();
    super.initState();
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return Color(int.parse(hexColor, radix: 16));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData (
          color: Colors.white,
        ),
        title: Text(widget.title, style: TextStyle(color: Colors.white)),
      ),

      body: isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : SingleChildScrollView(
          child: Container (
            padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
            child: Column(
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(documents['doc_name'], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                      ),

                      _text
                          ? Container(
                        padding: EdgeInsets.only(top: 15),
                        child: Text(documents['doc_description']),
                      )
                          : Container(
                        padding: EdgeInsets.only(top: 15),
                        child: Text(documents['doc_description'],
                          maxLines: 7,
                          overflow: TextOverflow.ellipsis,),
                      ),

                      Container(
                        padding: EdgeInsets.only(top: 15, bottom: 10),
                        child:  Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(documents['created'], style: TextStyle(color: Colors.grey),),

                            dataqq > dataq ?

                            Container() :

                            Container(
                                child: FlatButton(
                                    onPressed: (){
                                      setState((){
                                        _text = !_text;
                                      });
                                    },
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[

                                        InkWell(
                                          child: _text
                                              ? Text('Свернуть', style: TextStyle(color: Colors.blue),)
                                              : Text('Развернуть', style: TextStyle(color: Colors.blue),),
                                          onTap: (){
                                            setState((){
                                              _text = !_text;
                                            });
                                          },
                                        ),
                                      ],
                                    )
                                )
                            )

                          ],
                        ),
                      ),

                      Divider(height: 2,),

                      documents['document_link'] == null ?

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              width: MediaQuery.of(context).size.width / 2.3,
                              padding: EdgeInsets.all(10) ,
                              decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Text('Скачать документ', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                            ),
                          ),

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              width: MediaQuery.of(context).size.width / 2.3,
                              padding: EdgeInsets.all(10) ,
                              decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Text('Поделиться', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                            ),
                          )
                        ],
                      )

                          :

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              width: MediaQuery.of(context).size.width / 2.3,
                              padding: EdgeInsets.all(10) ,
                              decoration: BoxDecoration(
                                color: Color(0xff72b923),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Text('Скачать документ', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                            ),
                            onTap: (){
                              UrlLauncher.launch("https://advokat.vipsite.kz/" + documents['document_link']);
                            },
                          ),

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              width: MediaQuery.of(context).size.width / 2.3,
                              padding: EdgeInsets.all(10) ,
                              decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Text('Поделиться', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                            ),
                            onTap: (){
                              Share.share("https://advokat.vipsite.kz/" + documents['document_link']);
                            },
                          )

                        ],
                      )


                    ],
                  ),
                ),


              ],
            ),
          )
      ),

/*
        Container(
          color: Color(0xff4267b2),
          child:  ListTile(
            leading: Icon(Pravoved.call,
              color: Colors.white,),
            title: Text("8(000)000-000-0\n" + "Бесплатная консультация", style: TextStyle(color: Colors.white),),
            onTap: () {

            },
          ),
        ),

 */

      bottomNavigationBar: isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        decoration: BoxDecoration(
          color: Color(0xff4267b2),
        ),
        child: FlatButton(
            onPressed: () {
              UrlLauncher.launch("tel:$_number");
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Pravoved.call,
                    color: Colors.white
                ),
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                      "$_number\nБесплатная консультация",
                      style: TextStyle(
                          color: Colors.white
                      )
                  ),
                )
              ],
            )
        ),
      ),
    );
  }
}