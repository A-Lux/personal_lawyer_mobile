import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personallawyer/PageView.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'lang.dart';

class goodRevScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return goodRevScreenState();
  }
}

class goodRevScreenState extends State<goodRevScreen> {

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    getLang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return  Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),

          leading: Text(''),

          title: Text(trans('successful_post', _lang), style: TextStyle(color: Colors.white)),
        ),

        body:  SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
              child: Column(
                children: <Widget>[

                  Container(
                    padding: EdgeInsets.only(top: 50, bottom: 45),
                    child: Column(
                      children: <Widget>[
                        Image.asset("assets/q.png", width: 200, height: 200,),
                      ],
                    ),
                  ),

                  Align(
                    child: Text(trans('your_post_has_been_successfully_posted2', _lang),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'MyriadProRegular',
                        color: Color(0xff63ad22),
                      ),) ,
                  ),

                  /*   InkWell(
                    child: Container(
                        margin: EdgeInsets.only(top: 25),
                        width: 200,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Color(0xff63ad22),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text('Оплатить', style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      Pay();
                    },
                  ), */

                  InkWell(
                    child: Container(
                        margin: EdgeInsets.only(top: 25),
                        width: 220,
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          color: Color(0xff63ad22),
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(trans('go_to_main_page', _lang), style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) => PagerScreen()
                      ),);
                    },
                  ),

                ],
              ),
            )
        )
    );
  }
}