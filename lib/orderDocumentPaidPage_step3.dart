import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:personallawyer/finishedDocumentsPage.dart';
import 'package:personallawyer/paybox/PayCard.dart';
import 'package:personallawyer/paybox/url_launcher.dart';
import 'package:personallawyer/paybox/url_launcher_document.dart';
import 'package:personallawyer/seccessfulPublication.dart';
import 'package:personallawyer/seccessfulPublicationDocument.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'lang.dart';

class orderDocumentPaidPage_step3Screen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return orderDocumentPaidPage_step3ScreenState();
  }
}


class orderDocumentPaidPage_step3ScreenState extends State<orderDocumentPaidPage_step3Screen> {

  var data;

  String header_question;
  String text_question;
  String user_name;
  String phone_number;
  String user_city;
  String cityID;
  String user_email;
  String _api;
  String category;
  var isLoading = true;
  String _lang;

  bool btnIsActive = false;

  String url;
  var services;
  List<Widget>myList = new List<Widget>();
  ListView list;
  var _price = 0;

  bool isChecked = true;
  List<bool> checkboxes = new List<bool>();
//  List<int> prices = new List<int>();
//  Map prices = new Map();
  int prices;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void getInfo() async {

    SharedPreferences sp = await SharedPreferences.getInstance();

    header_question = sp.getString("titleTextDocument");
    text_question = sp.getString('descTextDocument');
    user_name = sp.getString('nameText');
    phone_number = sp.getString('phone_number_user');
    user_city = sp.getString('cityText');
    user_email = sp.getString('emailText');
    category = sp.getString('categoryDocument');
    cityID = sp.getString('cityID');

    print('IDcity: ' + cityID);

  }

  void sendRequest() async {

    final response = await http.post('https://advokat.vipsite.kz/api/document/paid_document',
        body: {
          'api_token': _api,
          'header_app_doc': header_question,
          'text_app_doc': text_question,
          'user_name': user_name,
          'phone_number': phone_number,
          'user_city': cityID,
          'user_email': user_email,
          'issue_price': jsonEncode(prices),
          'id_doc_category': category,
        });

    print(response.statusCode);

    if(response.statusCode == 200) {

      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString('id_doc', response.body);

      Pay();

//      Navigator.push(
//          context,
//          MaterialPageRoute(
//              builder: (context) => seccessfulPublicationDocumentScreen()));

//      Fluttertoast.showToast(
//        msg: jsonDecode(response.body),
//        timeInSecForIos: 3,
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.CENTER,
//        textColor: Colors.white,
//        backgroundColor: Colors.green,
//      );
    } else {
      print(jsonEncode(prices));
      print(_api);
      print(response.body);
      print('pidr');
    }
  }
  List<dynamic> cards = [];
  bool chooseCard = false;
  Map<String,dynamic> card;

  void getServices() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "https://advokat.vipsite.kz/api/$_api/question/valuable/documents/$_lang/";

    final response = await http.get(url);

    services = jsonDecode(response.body);

    print(services);

    setState(() {
      services;
      isLoading = false;
    });


//    for (int i = 0; i < services.length; i++) {
//
//    checkboxes.add(false);
//
//      myList.add(
//          new Column(
//            children: <Widget>[
//              Card(
//                child: new CheckboxListTile(
//                  title: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
//                      Text(services[i]["are_valuable"].toString() + ' тенге', style: TextStyle(color: Colors.green),),
//                    ],
//                  ),
//                 // value: checkboxes[i],
//                  value: checkboxes[i],
//                  onChanged: (value) {
//                    if(checkboxes[i] == false){
//                      prices["checkbox_"+services[i]['id'].toString()] = services[i]['id'];
//                      setState(() {
//                        _price += services[i]['are_valuable'];
//                        checkboxes[i] = true;
//                        print(prices.toString());
//                      });
//                    } else{
//                      prices.remove('checkbox_'+services[i]['id'].toString());
//                      setState(() {
//                        _price -= services[i]['are_valuable'];
//                        checkboxes[i] = false;
//                        print(prices.toString());
//                      });
//                    }
//                  },
//                ),
//              ),

//             new CheckboxListTile(
//                  title: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
//                      Text(services[i]["are_valuable"].toString() + ' тенге', style: TextStyle(color: Colors.green),),
//                    ],
//                  ),
////                  value: false,
////                  selected: checkboxes[i],
//                  value: services[i]['isCheck'],
//                  onChanged: (value) {
//                    setState(() {
//                      if(services[i]['isCheck'] == false){
//                        prices["checkbox_"+services[i]['id'].toString()] = services[i]['id'];
//                        _price += services[i]['are_valuable'];
//                        services[i]['isCheck'] = true;
////                        checkboxes[i] = true;
//                      } else {
//                        prices.remove('checkbox_'+services[i]['id'].toString());
//                        _price -= services[i]['are_valuable'];
//                        services[i]['isCheck'] = false;
////                        checkboxes[i] = false;
//                      }
//                    });
//                  },
//                ),
//
//            ],
//          )
//      );
//    }

  }

  void Pay() async {

    final SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString('apikey');
    var id = sp.getString('id_doc');

    print(id);
    print(_api);

    url = "https://advokat.vipsite.kz/api/$_api/payment/api_paybox_payment/$id/documents/";

    print(url);

    final response = await http.get(url);

    data = jsonDecode(response.body);
    print(data);

    setState(() {
      data;

      Navigator.push(context, MaterialPageRoute(builder: (context) => LaunchUrlDocument(url: data,)));

    });

  }
  void linkCard()async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    url = "https://advokat.vipsite.kz/api/$_api/payment/link-card";
    final response =await http.post(url);
    final body  = jsonDecode(response.body);
    Navigator.push(context,MaterialPageRoute(builder: (context)=>LaunchUrl(url:body['response']['pg_redirect_url'])));


  }
  void getCards()async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    url = "https://advokat.vipsite.kz/api/$_api/payment/cards";
    final response = await http.get(url);
    final body =  jsonDecode(response.body);

    setState(() {
      cards = body['cards'];
      print(cards);
    });

  }

  void initState() {
    super.initState();
    getInfo();
    getServices();
    getLang();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData (
          color: Colors.white,
        ),
        title: Text(trans('document_type', _lang), style: TextStyle(color: Colors.white)),

        actions: <Widget>[
          FlatButton(
              child: Container(
                child: Text(trans('step3of3', _lang), style: TextStyle(color: Colors.white),),
              )
          )
        ],
      ),

      body: isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          :  SingleChildScrollView(
        child: Container (
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 10),
                child: Text(trans('what_do_you_want_to_order', _lang), style: TextStyle(fontSize: 17), textAlign: TextAlign.left,),
              ),


              Column(
                children: myList,
              ),




//              Container(
//                height: 110,
//                child: ListView.builder(
//                    itemCount: services.length,
//                    itemBuilder: (context, i){
////                      prices.add(0);
////                      checkboxes.add(false);
//                      return new RadioListTile(
//                          title: Row(
//                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                            children: <Widget>[
//                              Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
//                              Text(services[i]["are_valuable"].toString() + ' тенге', style: TextStyle(color: Colors.green),),
//                            ],
//                          ),
//                         // selected: checkboxes[i],
//                          value: services[i]['isCheck'],
//                          onChanged: (value) {
//                            setState(() {
//                              prices = services[i]['id'];
//                              _price = services[i]['are_valuable'];
////                              isChecked = !isChecked;
//                              isChecked = true;
//                              print(prices.toString());
//                            });
//                          }
//                      );
//                    }),
//              ),

              Container(
                padding: EdgeInsets.only(right: 15, left: 15),
                height: 160,
                child: ListView.builder(
                    itemCount: services.length,
                    itemBuilder: (context, i){
//                      prices.add(0);
//                      checkboxes.add(false);
                      return new Card(
                        child: RadioListTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
                            ],
                          ),
                          subtitle: Text(services[i]["are_valuable"].toString() + trans('tenge', _lang), style: TextStyle(color: Colors.black),),
                          value: services[i]['isCheck'],
                          groupValue: isChecked ,
                          onChanged: (value) {
                            setState(() {
                              btnIsActive = true;
                              for(var i = 0; i < services.length; i++) {
                                services[i]['isCheck'] = false;
                              }
//                              isChecked = true;
                              services[i]['isCheck'] = true;

                              prices = services[i]['id'];
                              _price = services[i]['are_valuable'];
                             // print(prices.toString());
                            });
                          },
                          activeColor: Colors.blue,
                        ),
                      );
                    }),
              ),

              Container(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 10),
                  child: Row(
                    children: <Widget>[
                      Text(trans('total', _lang), style: TextStyle(fontSize: 17), textAlign: TextAlign.left,),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text('${_price}' + trans('tenge', _lang), style: TextStyle(fontSize: 17), textAlign: TextAlign.left,),
                      )
                    ],
                  )
              ),
              GestureDetector(

                child: Column(
                  children: [
                    GestureDetector(
                      onTap: (){
                        chooseCard = !chooseCard;
                        getCards();

                      },
                      child: Container(
                        padding: const EdgeInsets.only(top:20,left:20,right:20),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black,),
                              borderRadius: BorderRadius.circular(10)


                          ),
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Center(
                                child: Text(card != null ? card['card_hash'] :'Выбрать карту'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    chooseCard == true ? Container(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10),

                      child: Container(
                        height: 150,
                        child: ListView.builder(
                            itemCount: cards.length,
                            itemBuilder: (context,index){
                              return Column(
                                children: [
                                  GestureDetector(
                                    onTap:(){
                                      setState(() {
                                        chooseCard = false;
                                        card = cards[index];
                                        print(card);
                                      });

                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: index != 0 ? Border(top: BorderSide(color: Colors.black )) : Border(top:BorderSide(color:Colors.white)),
                                      ) ,

                                      padding: const EdgeInsets.symmetric(vertical: 15,),
                                      child: Center(
                                        child: Text('${cards[index]['card_hash']}',style: TextStyle(fontSize: 15),),

                                      ),
                                    ),
                                  ),
                                  index == cards.length-1 ?
                                  GestureDetector(
                                    onTap:()async{
                                      linkCard();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: cards.length != 0 ? Border(top: BorderSide(color: Colors.black )) : Border.all(color:Colors.white),
                                      ) ,

                                      padding: const EdgeInsets.symmetric(vertical: 15,),
                                      child: Center(
                                        child: Text('Добавить карту',style: TextStyle(fontSize: 15),),

                                      ),
                                    ),
                                  ): SizedBox.shrink()

                                ],
                              );
                            }
                        ),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)
                        ),
                      ),
                    ) : SizedBox.shrink()
                  ],
                ),
              ),


//                  Container(
//                    padding: EdgeInsets.only(top: 20, left: 15, right: 15),
//                    child: Column(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//                        InkWell(
//                          child: Container(
//                              width: 200,
//                              padding: EdgeInsets.all(10),
//                              decoration: BoxDecoration(
//                                color: Colors.green,
//                                borderRadius: BorderRadius.circular(10),
//                              ),
//                              child: Align(
//                                alignment: Alignment.center,
//                                child: Text('Оплатить', style: TextStyle(color: Colors.white),),
//                              )
//                          ),
//                          onTap: (){
//
//                          },
//                        ),
//                      ],
//                    ),
//                  )

            ],
          ),
        ),
      ),

      bottomNavigationBar: Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: FlatButton(
            onPressed: btnIsActive ? () {
              if(card != null){
                print(card);
                payWithCard();
              }else{
                sendRequest();
              }
            }
            : null,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.account_balance_wallet,
                    color: Colors.white
                ),
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                      trans('to_pay', _lang),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15
                      )
                  ),
                )
              ],
            )
        ),
      ),

    );


  }

  void payWithCard()async {
    final response = await http.post('https://advokat.vipsite.kz/api/document/paid_document',
        body: {
          'api_token': _api,
          'header_app_doc': header_question,
          'text_app_doc': text_question,
          'user_name': user_name,
          'phone_number': phone_number,
          'user_city': cityID,
          'user_email': user_email,
          'issue_price': jsonEncode(prices),
          'id_doc_category': category,
        });

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    print(response.body);
    if(response.statusCode == 200) {
      final url = 'https://advokat.vipsite.kz/api/$_api/payment/pay-by-card';
      var id = sp.getString('id');
      final payResponse = await http.post(url,body: {
        'user_card_id' : card['id'].toString(),
        'pay_id' : id,
        'description' : 'questions'
      });
      final mainPayment = jsonDecode(payResponse.body)['payment'];
      Navigator.push(context,MaterialPageRoute(builder: (context)=> PayCard(htmlData:mainPayment,isDocumentSale: true,)));

    }
  }
}