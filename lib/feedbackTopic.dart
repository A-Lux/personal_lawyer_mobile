import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:personallawyer/feedbackSend.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'lang.dart';

class FeedbackTopicScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return FeedbackTopicScreenState();
  }
}

class FeedbackTopicScreenState extends State<FeedbackTopicScreen> {

  String _api;
  var url;
  var topics;
  var isLoading = true;
  List<Widget>myList = new List();
  ListView list;

  void getTopics() async{

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "https://advokat.vipsite.kz/api/$_api/topics_for_massage/$_lang/";

    final response = await http.get(url);

    topics = jsonDecode(response.body);
    print(topics);

    setState(() {
      topics;
      isLoading = false;
    });

    for (int i = 0; i < topics.length; i++) {
      myList.add(
          new Column(
            children: <Widget>[
              Container(
                child: ListTile(
                  title: Text(topics[i]["topics_text"]),
                  trailing: Icon(Icons.chevron_right, color: Colors.grey),
                  onTap: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FeedbackSend(id: topics[i]["id"], title: topics[i]["topics_text"],)));
                  },
                ),
              ),
              Divider(height: 2.0,),
            ],
          )

      );
    }

  }

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    getTopics();
    getLang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(trans('theme_of_appeal', _lang), style: TextStyle(color: Colors.white)),
        ),

        body: isLoading
            ? Center(
            child: CircularProgressIndicator())
            : SingleChildScrollView(
          child: Align(
              alignment: Alignment.center,
              //  padding: EdgeInsets.all(10),
              child: Column(
                children: myList,
              )
          ),
        )
    );
  }
}