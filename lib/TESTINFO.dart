import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:personallawyer/QuestionPage.dart';
import 'navDrawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class MainMenuScreen1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(

        ),
        drawer: NavDrawer(),
        body: MainMenuBody1()
    );
  }
}

class MainMenuBody1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainMenuBodyState1();
  }
}

class MainMenuBodyState1 extends State<MainMenuBody1> {

  String _api;
  String _lang;

  List<Posts> _listMap = [];
  var loading = false;
  var isLoading = true;

  ScrollController _scrollController;
  var postsCount = 0;
  var contentIsDone = false;

  Future<Null> fetchData() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    if (mounted) {
      setState(() {
        loading = true;
      });
    }


    final response = await http.get("https://advokat.vipsite.kz/api/$_api/question/questions_on_the_project/$postsCount/$_lang");
    print(response.statusCode);
    if (response.statusCode == 200) {

      var data = jsonDecode(response.body);
      if (mounted && data != null) {
        setState(() {
          for (Map i in data) {
            _listMap.add(Posts.formJson(i));
            loading = false;

            isLoading = false;
          }
        });
      }
    } else {
      contentIsDone = true;
    }
  }

  @override
  void initState(){
    _scrollController = new ScrollController()..addListener(_scrollListener);
    fetchData();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    /*

    return SingleChildScrollView(
      child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[

              ListTile(
                title: Container(
                  padding: EdgeInsets.only(top: 5, bottom: 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                      Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                  Text(' 0 ответов', style: TextStyle(fontSize: 14),),
                                ],
                              ),
                            ),
                            Container(
                              child: Text('Сегодня, 11:22', style: TextStyle(color: Colors.grey, fontSize: 14),),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                onTap:() {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => QuestionPageScreen()));
                },
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              Divider(height: 2.0,),

              ListTile(
                  title: Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                    Text(' 1 ответов', style: TextStyle(fontSize: 14),),
                                  ],
                                ),
                              ),
                              Container(
                                child: Text('Сегодня, 10:40', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  trailing: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Icon(Icons.arrow_forward_ios),
                  ),
                onTap:() {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => QuestionPage1Screen()));
                },
              ),
              Divider(height: 2.0,),

              ListTile(
                  title: Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child:  Text('Цена вопроса: 1500 тг', style: TextStyle(fontSize: 15, color: Colors.green),),
                        ),
                        Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                    Text(' 3 ответов', style: TextStyle(fontSize: 14),),
                                  ],
                                ),
                              ),
                              Container(
                                child: Text('Вчера, 12:02', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  trailing: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Icon(Icons.arrow_forward_ios),
                  )
              ),
              Divider(height: 2.0,),

              ListTile(
                  title: Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child:  Text('Цена вопроса: 3600 тг', style: TextStyle(fontSize: 15, color: Colors.green),),
                        ),
                        Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                    Text(' 8 ответов', style: TextStyle(fontSize: 14),),
                                  ],
                                ),
                              ),
                              Container(
                                child: Text('23.12.2019, 10:44', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  trailing: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Icon(Icons.arrow_forward_ios),
                  )
              ),
              Divider(height: 2.0,),

              ListTile(
                  title: Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child:  Text('Цена вопроса: 3600 тг', style: TextStyle(fontSize: 15, color: Colors.green),),
                        ),
                        Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                    Text(' 4 ответов', style: TextStyle(fontSize: 14),),
                                  ],
                                ),
                              ),
                              Container(
                                child: Text('21.12.2019, 09:44', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  trailing: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Icon(Icons.arrow_forward_ios),
                  )
              ),
              Divider(height: 2.0,),

              ListTile(
                  title: Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child:  Text('Цена вопроса: 2600 тг', style: TextStyle(fontSize: 15, color: Colors.green),),
                        ),
                        Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                    Text(' 2 ответов', style: TextStyle(fontSize: 14),),
                                  ],
                                ),
                              ),
                              Container(
                                child: Text('20.12.2019, 23:35', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  trailing: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Icon(Icons.arrow_forward_ios),
                  )
              ),
              Divider(height: 2.0,),

              ListTile(
                  title: Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child:  Text('Цена вопроса: 2600 тг', style: TextStyle(fontSize: 15, color: Colors.green),),
                        ),
                        Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                    Text(' 2 ответов', style: TextStyle(fontSize: 14),),
                                  ],
                                ),
                              ),
                              Container(
                                child: Text('20.12.2019, 23:35', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  trailing: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Icon(Icons.arrow_forward_ios),
                  )
              ),
              Divider(height: 2.0,),


              ListTile(
                  title: Container(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child:  Text('Цена вопроса: 2600 тг', style: TextStyle(fontSize: 15, color: Colors.green),),
                        ),
                        Text('Вопрос права собственности на землю по приобритательной давности?', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                    Text(' 2 ответов', style: TextStyle(fontSize: 14),),
                                  ],
                                ),
                              ),
                              Container(
                                child: Text('20.12.2019, 23:35', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  trailing: Container(
                    height: MediaQuery.of(context).size.height / 2,
                    child: Icon(Icons.arrow_forward_ios),
                  )
              ),
              Divider(height: 2.0,),




            ],
          )
      ),
    );

    */

    return ListView.builder(
      // controller: _controller,
        controller: _scrollController,
        itemCount: _listMap.length,
        itemBuilder: (context, i) {
          final a = _listMap[i];
          return Column(
            children: <Widget>[

              ListTile(
                title: Container(
                  padding: EdgeInsets.only(top: 5, bottom: 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      a.issue_price == 'null' ? Container() :
                      Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Text('Цена вопроса: ' + a.issue_price, style: TextStyle(fontSize: 15, color: Colors.green),),
                      ),
                      Text(a.header_question, style: TextStyle(fontWeight: FontWeight.bold),),
                      Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
                                  Text(a.new_answers_count + ' ответов', style: TextStyle(fontSize: 14),),
                                ],
                              ),
                            ),
                            Container(
                              child: Text(a.created, style: TextStyle(color: Colors.grey, fontSize: 14),),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                onTap:() {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => QuestionPageScreen(id_question: a.id,)));
                },
                trailing: Icon(Icons.arrow_forward_ios),
              ),

              Divider(
                height: 0.2,
              ),

              i == _listMap.length - 1 ?
              contentIsDone
                  ?
              Container()
                  :
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                alignment: Alignment.bottomCenter,
                child: CircularProgressIndicator(),
              )
                  :
              Container()

            ],
          );
        });


  }

  void _scrollListener() {

    if(_scrollController.position.extentAfter < 100 && contentIsDone == false){
      print(_scrollController.position.extentAfter);

      setState(() {
        isLoading = false;
        fetchData();
        postsCount += 20;
        // await
      });
    }
  }
}

class Posts {
  final String id;
  final String header_question;
  final String city_question;
  final String text_question;
  final String mobile_users_id;
  final String status_question;
  final String days_of_waiting;
  final String created;
  final String new_answers_count;
  final String issue_price;

  Posts({this.id, this.header_question, this.city_question, this.text_question, this.mobile_users_id, this.status_question, this.days_of_waiting, this.created, this.new_answers_count, this.issue_price});

  factory Posts.formJson(Map <String, dynamic> json){
    //  print(json.toString());
    return new Posts(
      id: json['id'].toString(),
      header_question: json['header_question'].toString(),
      city_question: json['city_question'].toString(),
      text_question: json['text_question'].toString(),
      mobile_users_id: json['mobile_users_id'].toString(),
      status_question: json['status_question'].toString(),
      days_of_waiting: json['days_of_waiting'].toString(),
      created: json['created'].toString(),
      new_answers_count: json['new_answers_count'].toString(),
      issue_price: json['issue_price'].toString(),
    );
  }
}
