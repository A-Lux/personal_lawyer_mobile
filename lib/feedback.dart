import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:personallawyer/feedbackTopic.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class FeedbackScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FeedbackScreenState();
  }
}

class FeedbackScreenState extends State<FeedbackScreen> {

  String _api;
  var url;
  var data;
  var isLoading = true;

  void checkNumber() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

    url = "https://advokat.vipsite.kz/api/$_api/phone_feedback/";

    final response = await http.get(url);

    data = jsonDecode(response.body);

    setState(() {
      data;

      isLoading = false;
    });

  }

  void initState() {
    checkNumber();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text("Обратная связь", style: TextStyle(color: Colors.white)),
        ),

        body:  isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            :  SingleChildScrollView(
          child: Align(
              alignment: Alignment.center,
              //  padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[


                  ListTile(
                    title: Text('Написать сообщение'),
                    trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FeedbackTopicScreen()));
                    },
                  ),
                  Divider(height: 2.0,),
//
//                 Container(
//                   padding: EdgeInsets.only(top:10),
//                   child: ListTile(
//                     title: Text("Написать сообщение", style: TextStyle(fontFamily: 'MyriadProRegular'),),
//                     trailing: Icon(Icons.chevron_right, color: Colors.grey,),
//                     onTap: () {
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) => FeedbackTopicScreen()));
//                     },
//                   ),
//                 ),
//
//                  Divider(height: 2,)

                  /* Divider(height: 2,),

                  Container(
                    padding: EdgeInsets.only(top:15),
                    width: 280,
                    child: Text('Технические вопросы Вы можете задать по телефону:', style: TextStyle(fontSize: 17, fontFamily: 'MyriadProRegular'), textAlign: TextAlign.center,),
                  ),

                  InkWell(
                    child: Container(
                      width: 180,
                      padding: EdgeInsets.only(top:15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[

                          Icon(Icons.call, size: 35, color: Colors.blue),
                          Text(data, style: TextStyle(fontSize: 20, color: Colors.blue, fontFamily: 'MyriadProRegular'),),

                        ],
                      ),

                    ),
                    onTap: (){
                      UrlLauncher.launch("tel:$data");
                    },
                  ),

                  Container(
                    padding: EdgeInsets.only(top: 15),
                    child: Text("Звонок для Вас бесплатный", style: TextStyle(color: Colors.grey, fontSize: 17, fontFamily: 'MyriadProRegular'),),
                  ) */

                ],
              )
          ),
        )
    );
  }
}