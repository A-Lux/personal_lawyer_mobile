import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:personallawyer/PageView.dart';
import 'package:personallawyer/main.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

import 'feedbackTopic.dart';

class MainStartScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainStartScreenState();
  }
}

class MainStartScreenState extends State<MainStartScreen> {

  bool rus = true;

  String signature = "{{ app signature }}";
  StreamSubscription _subscription;

  void languageSetted(String lang) async {
    SharedPreferences spref = await SharedPreferences.getInstance();
    //spref.setString("language", lang);
  }

  TextEditingController _phoneC =
  new MaskedTextController(mask: '+7(000)000-00-00');
  TextEditingController _smsC = new TextEditingController();



  Timer _timer;
  int _start = 60;


  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) => setState(
            () {
          if (_start < 1) {
            timer.cancel();
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  double setTop() {
    if(MediaQuery.of(context).size.width*MediaQuery.of(context).devicePixelRatio > 700){
      return 50;
    } else {
      return 65;
    }
  }

  void GetUser() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _phoneC.text = prefs.getString('phoneI');
  }

  void test(){
    yg = _start == 0 ? false : true;
  }

  bool yg = true;

  @override
  void dispose(){
    _timer.cancel();
    super.dispose();
  }

  void initState() {
    startTimer();
    GetUser();
    languageSetted("rus");
    super.initState();
  }

  Widget build(BuildContext context) {
    test();
    return Container(
        decoration: new BoxDecoration(

          image: new DecorationImage(

            image: new AssetImage("assets/fon.png"),
            fit: BoxFit.cover,


          ),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Container(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: setTop(), left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Text("Личный адвокат", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white, fontFamily: 'MyriadProRegular'),),
                        ),



                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              /*

                        Container(
                          child: FlatButton(
                            child: Text('Войти', style: TextStyle(fontSize: 18, color: Colors.white, fontFamily: 'MyriadProRegular'),),
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginScreen()));
                            }
                          ),
                        ),
                        Icon(Icons.arrow_forward_ios, color: Colors.white,)


                        */

                            ],
                          ),
                        )

                      ],
                    ),
                  ),


                  Container(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/3.7, left: 16.0, right: 16.0),
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[

                                    Container(
                                      alignment: Alignment.topLeft,
                                      padding: EdgeInsets.only(top:10, bottom:5),
                                      child: Text("Номер телефона",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                    TextField(
                                      onTap: (){
                                        if(_phoneC.text.isEmpty){
                                          _phoneC.text = "+7";
                                        }
                                      },
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        color: Colors.white,
                                      ),
                                      controller: _phoneC,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                          hintText: "+7(***)***-**-**",
                                          hintStyle: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18.0,
                                          ),
                                          //                           border: InputBorder.none,
                                          //labelText: _phone,
                                          contentPadding: EdgeInsets.all(7.0),
                                          labelStyle: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w100,
                                            fontSize: 18.0,
                                          )),
                                    ),

                                    TextFormField(
                                      keyboardType: TextInputType.number,
                                      style: TextStyle(
                                        fontSize: 18.0,
                                        color: Colors.white,
                                      ),
                                      controller: _smsC,
                                      obscureText: true,
                                      decoration: InputDecoration(
                                          hintText: "Введите SMS-код",
                                          hintStyle: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18.0,
                                          ),
                                          //                           border: InputBorder.none,
                                          //labelText: _pass,
                                          contentPadding: EdgeInsets.all(7.0),
                                          labelStyle: TextStyle(
                                            fontWeight: FontWeight.w100,
                                            color: Colors.white,
                                            fontSize: 18.0,
                                          )
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.topLeft,
                                      padding: EdgeInsets.only(top:15, bottom:5),
                                      child: yg ? Text("Отправить повторный запрос через: " + "$_start",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(color: Colors.white),
                                      )
                                          :
                                      Builder(
                                        builder: (context) => InkWell(
                                          child: Text("Отправить код повторно",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                          onTap: () {
                                            _start = 60;
                                            yg = true;
                                            startTimer();
                                            checkUserSend();
                                          },
                                        ),
                                      ),
                                    ),
                                    Builder(
                                      builder: (context) => Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Expanded(
                                            child: Container(
                                              padding:
                                              EdgeInsets.only(top: 15, left: 32, right: 32),
                                              //width: MediaQuery.of(context).size.width,
                                              child: RaisedButton(
                                                color: Color(0xff4267b2),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(35),
                                                ),
                                                onPressed: () {
                                                  checkUser(context);
                                                },
                                                child: Text("Подтвердить SMS-код",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 15.0,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(top: 10, bottom: 40),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Builder(
                                            builder: (context) => InkWell(
                                              child:  Text("Ввести другой номер",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) => MyHomePage()));
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),



                        ],
                      )
                  ),
                ],
              ),
            ),

            /*

            bottomNavigationBar: Container(
              padding: EdgeInsets.only(bottom: 30,right: 25, left: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  InkWell(
                    child: Container(
                      decoration: new BoxDecoration(
                        color: Color(0xffed5e68),
                        borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10),
                            bottom: Radius.circular(10)
                        ),
                      ),
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Icon(Icons.chat_bubble, size: 35, color: Colors.white,),
                            padding: EdgeInsets.only(right: 10),
                          ),
                          Text('Заказать услугу', style: TextStyle(color: Colors.white, fontFamily: 'MyriadProRegular'),),
                        ],
                      ),
                    ),
                    onTap: (){

                    },
                  ),



                  InkWell(
                    child: Container(
                      decoration: new BoxDecoration(
                        color: Color(0xff4267b2),
                        borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10),
                            bottom: Radius.circular(10)
                        ),
                      ),
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Icon(Icons.call, size: 35, color: Colors.white,),
                            padding: EdgeInsets.only(right: 10),
                          ),
                          Text('Позвонить \n8(800)450-95-44   ', style: TextStyle(color: Colors.white, fontFamily: 'MyriadProRegular'),),
                        ],
                      ),
                    ),
                    onTap: (){

                    },
                  ),


                ],

              ),

            )

            */

        ));
  }

  Future loginEnter() async {
    print("Hello");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("logged", true);
    Route route = MaterialPageRoute(builder: (context) => PagerScreen());
    Navigator.pushReplacement(context, route);
  }


  Future checkUserSend() async {

    if(_phoneC.text.isNotEmpty){

      var url = "https://advokat.vipsite.kz/api/login/send";

      final response = await http.post(url, body: {"phone": _phoneC.text});



      if (response.statusCode == 200) {

      } else {
        Fluttertoast.showToast(
          msg: "Произошла ошибка",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
        );
        throw Exception('Failed to load files');
      }
    } else {
      Fluttertoast.showToast(
        msg: "Введите номер телефона.",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
  }


  var apikey;

  Future checkUser(BuildContext con) async {

    var stat = await OneSignal.shared.getPermissionSubscriptionState();

    if(_phoneC.text.isNotEmpty){

      var url = "https://advokat.vipsite.kz/api/login/verify";

      final response = await http.post(url,
          body: {
        "phone": _phoneC.text,
        "code": _smsC.text,
        "app_code" : stat.subscriptionStatus.userId
        });

      print(response.statusCode);
      print(response.body);
      print(stat.subscriptionStatus.userId);

      if (response.statusCode == 200) {

        apikey = json.decode(response.body);

        SharedPreferences sp = await SharedPreferences.getInstance();
        sp.setString('apikey', apikey[0]["api"]);

       // print(apikey);
       // setUserInfo();
        loginEnter();

      } else {
        Fluttertoast.showToast(
          msg: "Вы ввели не правильно проверочный СМС код.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
        );
        throw Exception('Failed to load files');
      }
    } else {
      Fluttertoast.showToast(
        msg: "Введите номер телефона",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
  }

  void setUserInfo() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("apikey", apikey[0]["api"]);
  }


}