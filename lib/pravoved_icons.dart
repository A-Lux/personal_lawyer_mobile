/// Flutter icons Pravoved
/// Copyright (C) 2019 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  Pravoved
///      fonts:
///       - asset: fonts/Pravoved.ttf
///
/// 
///
import 'package:flutter/widgets.dart';

class Pravoved {
  Pravoved._();

  static const _kFontFam = 'Pravoved';

  static const IconData call = const IconData(0xe800, fontFamily: _kFontFam);
  static const IconData next = const IconData(0xe803, fontFamily: _kFontFam);
  static const IconData star = const IconData(0xe805, fontFamily: _kFontFam);
  static const IconData upload = const IconData(0xe806, fontFamily: _kFontFam);
  static const IconData message = const IconData(0xe807, fontFamily: _kFontFam);
  static const IconData vopros = const IconData(0xe808, fontFamily: _kFontFam);
  static const IconData voprosV = const IconData(0xe809, fontFamily: _kFontFam);
  static const IconData folderber = const IconData(0xe80a, fontFamily: _kFontFam);
  static const IconData envelope = const IconData(0xe80b, fontFamily: _kFontFam);
  static const IconData settings = const IconData(0xe80c, fontFamily: _kFontFam);
}
