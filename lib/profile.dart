import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:personallawyer/editProfile.dart';
import 'package:personallawyer/feedbackTopic.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'myquestion.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfileScreenState();
  }
}

class ProfileScreenState extends State<ProfileScreen> {

  String data;
  String _api;
  var url;
  var user;

  var isLoading = true;

  bool _notification = false;

  void checkUser() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");


    url = "https://advokat.vipsite.kz/api/$_api/about_you/user/user_data";

    final response = await http.get(url);

    user = jsonDecode(response.body);

    print(user);

    setState(() {
      isLoading = false;
    });

  }

  void initState() {
    checkUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text("Настройки", style: TextStyle(color: Colors.white)),
        ),

      body: isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          :  SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 10.0, left: 12.0, right: 12.0),
          child: Container(
            child: Card(
              child: Column(
                children: <Widget>[
                  Container(
                    //color: Colors.red,
                      padding: EdgeInsets.only(top:20.0, bottom: 30.0),
                      child:  CircularProfileAvatar(
                        'https://advokat.vipsite.kz/' + user['image_link'],
                        radius: 60,
                        backgroundColor: Colors.transparent,
                        borderWidth: 3,
                        //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
                        borderColor: Colors.white,
                        elevation: 5.0,
                        //foregroundColor: Colors.white10.withOpacity(0.5),
                        cacheImage: true,
                        onTap: () {
                       // getImage(context);
                        },
                        showInitialTextAbovePicture: true,
                      )
                  ),

                  Container(
                    padding: EdgeInsets.only(bottom: 25.0),
                    child: Text(user['user_name'], textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),),
                  ),

                  Divider(height: 2.0),

//                  ListTile(
//                    title: Text("Изменить информацию"),
//                    onTap: () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) =>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   editProfileScreen()));
//                    },
//                    trailing: Icon(Icons.chevron_right, color: Colors.green,),
//                  ),
//                  Divider(
//                    height: 2.0,
//                  ),

                  ListTile(
                    title: Text("Мои вопросы"),
                    trailing: Icon(Icons.chevron_right, color: Colors.green),
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MyQuestionScreen()));
                    },
                  ),

                  Divider(height: 2.0),

                  SwitchListTile(
                    value: _notification,
                    activeColor: Colors.green,
                    title: Text('Уведомления'),
                    onChanged: (value){
                      _notification = value;
                      print(value ? 'DA' : 'NET');
                    },
                  ),

                  Divider(
                    height: 2.0,
                  ),

                  ListTile(
                    title: Text('Обратная связь'),
                    trailing: Icon(Icons.chevron_right, color: Colors.green),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FeedbackTopicScreen()));
                    },
                  ),

                  Divider(
                    height: 2.0,
                  ),

                  ListTile(
                    title: Text("Выйти из приложения"),
                    trailing: Icon(Icons.exit_to_app, color: Colors.red),
                    onTap: () {
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}