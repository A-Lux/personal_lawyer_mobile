import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:share/share.dart';
import 'lang.dart';

class TellAboutUsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TellAboutUsScreenState();
  }
}

class TellAboutUsScreenState extends State<TellAboutUsScreen> {

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    getLang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(trans('tell_us_about_us', _lang), style: TextStyle(color: Colors.white)),
        ),

        body: SingleChildScrollView(
          child: Align(
            alignment: Alignment.center,
            //  padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[



                Container(
                  padding: EdgeInsets.only(top: 90),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      Container(
                        padding: EdgeInsets.only(bottom: 25),
                        child: Column(
                          children: <Widget>[
                            Image.asset("assets/about.png", width: 230, height: 230,),
                          ],
                        ),
                      ),

                      Align(
                        child: Text(trans('info_text5', _lang),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 17,
                              fontFamily: 'MyriadProRegular'
                          ),) ,
                      ),

                     /* Container(
                          padding: EdgeInsets.only(top: 25),
                          width: 210,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[

                                InkWell(
                                  child: Image.asset("assets/vk.png", width: 65, height: 65,),
                                  onTap: (){
                                    Share.share("Поделиться приложением, ссылка: url");
                                  },
                                ),
                                InkWell(
                                  child: Image.asset("assets/facebook.png", width: 65, height: 65,),
                                  onTap: (){
                                    Share.share("Поделиться приложением, ссылка: url");
                                  },
                                ),
                                InkWell(
                                  child: Image.asset("assets/twitter.png", width: 65, height: 65,),
                                  onTap: (){
                                    Share.share("Поделиться приложением, ссылка: url");
                                  },
                                )

                                   ],
                          ),
                      )
          */

                    ],
                  )
                )


                ],
              )
          ),
        ),

      bottomNavigationBar: Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: FlatButton(
            onPressed: () {
              Share.share("Поделиться приложением Ваш личный адвокат, ссылка: https://play.google.com/store/apps/details?id=kz.itgroup.personallawyer&hl=ru");
//              Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (context) => QuestionPay(price:_price)));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                      trans('share_this', _lang),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15
                      )
                  ),
                )
              ],
            )
        ),
      ),
    );
  }
}